O_DIR = obj/
EXEC_DIR = bin/
SRC_DIR = src/
HEADERS_DIR = include/
TEST_DIR = tests/
CC = gcc
CFLAGS = -g -Wall
SOURCES = $(wildcard $(SRC_DIR)*.c)
OBJECTS = $(patsubst $(SRC_DIR)%.c, $(O_DIR)%.o, $(SOURCES))
TESTSRC = $(wildcard $(TEST_DIR)*.c)
TESTEXEC = $(patsubst $(TEST_DIR)%.c, $(TEST_DIR)%.test, $(TESTSRC))
TESTS = $(patsubst $(TEST_DIR)%.test, %.test, $(TESTEXEC))
EXEC = spacker
OBJECTSNOEXEC := $(filter-out $(O_DIR)$(EXEC).o, $(OBJECTS))
INIT_DIR = @mkdir -p

vpath %.c src
vpath %.o obj
vpath %.h include
vpath main bin

all : $(EXEC_DIR)$(EXEC) $(TESTEXEC)

exec: $(EXEC_DIR)$(EXEC)

memcheck: $(EXEC_DIR)$(EXEC)

$(EXEC_DIR)$(EXEC) : $(OBJECTS)
	@echo -e "\033[1;33m------------------------Linking\033[0m"
	$(INIT_DIR) $(EXEC_DIR)
	$(CC) $(CFLAGS) -o $@ $^

$(O_DIR)$(EXEC).o : $(SRC_DIR)$(EXEC).c
	@echo -e "\e[1;33m------------------------Compiling $@\033[0m"
	$(CC) $(CFLAGS) -I $(HEADERS_DIR) -c  $< -o $@

$(O_DIR)%.o : $(SRC_DIR)%.c $(HEADERS_DIR)%.h
	@echo -e "\033[1;33m------------------------Compiling $@\033[0m"
	$(INIT_DIR) $(O_DIR)
	$(CC) $(CFLAGS) -I $(HEADERS_DIR) -c $< -o $@

# Tests

test: $(TESTS)

%.test: $(TEST_DIR)%.test
	@echo -e "\033[1;33m------------------------Testing $@\033[0m"
	$(TEST_DIR)$@

$(TEST_DIR)%.test: $(TEST_DIR)%.c $(OBJECTSNOEXEC)
	@echo -e "\033[1;33m------------------------Compiling test $@\033[0m"
	$(CC) $(CLFAGS) -I $(HEADERS_DIR) -I $(TEST_DIR) -o $@ $< $(OBJECTSNOEXEC)

#Clean et memcheck
memcheck : $(EXEC_DIR)$(EXEC)
	@echo -e "\033[1;33m ------------------------Stating memory leak check for $(EXEC)\033[0m"
	valgrind --leak-check=full $<


clean :
	@echo -e "\033[1;33m------------------------Cleaning\033[0m"
	rm $(OBJECTS) $(EXEC_DIR)$(EXEC) $(TEST_DIR)*.test