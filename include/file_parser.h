#pragma once

#include <stdio.h>

#define MAX_BUFFER 160

void parse_file(FILE *f) {
  char buf[MAX_BUFFER];
  int i;
  while ((fgets(buf, MAX_BUFFER, f)) != NULL) {
    i = 0;
    while (buf[i] != '\0') {
      printf("%c", buf[i]);
      i++;
    }
  }
}