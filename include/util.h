#pragma once

struct member {
  char *type;
  char *name;
};

struct structure {
  char *name;
  struct member *fields;
};