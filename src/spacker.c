#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "file_parser.h"
#include "util.h"

int main(int argc, char **argv) {
  if (argc != 2) {
    fprintf(stderr, "ERROR usage: %s filename\n", argv[0]);
    exit(1);
  }
  FILE *toChange;

  if ((toChange = fopen(argv[1], "r")) == NULL) {
    fprintf(stderr, "ERROR loading: %s\n", argv[1]);
    perror("fopen");
    exit(1);
  }

  parse_file(toChange);

  if (fclose(toChange) != 0) {
    perror("fclose");
    exit(1);
  }

  return 0;
}