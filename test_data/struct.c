#include <stddef.h>
#include <stdio.h>

struct foo {
  int a;
  char b;
  long double c;
};

struct foo_packed {
  long double c;
  int a;
  char b;
};

int main(int argc, char **argv) {
  char c = 'l';
  struct foo a = {1, 'a', 1.};
  struct foo_packed b = {1., 1, 'a'};
  printf("{%d,%d,%d} -> %d vs {%d,%d,%d} -> %d\n", offsetof(struct foo, a),
         offsetof(struct foo, b), offsetof(struct foo, c),
         offsetof(struct foo, c) + sizeof(long double),
         offsetof(struct foo_packed, c), offsetof(struct foo_packed, a),
         offsetof(struct foo_packed, b),
         offsetof(struct foo_packed, b) + sizeof(char));
  return 0;
}